#!/usr/bin/env node

// Using worker_threads together with async.parallel[Limit] to ensure
// the operation is multithreaded and uses the full power of a multicore machine.

const { Worker } = require('worker_threads')
const path = require('path')
const os = require('os')
const parallelLimit = require('async/parallelLimit')

// number of jobs to run will be the number of CPU cores
const limit = os.cpus().length

const workerScript = path.join(__dirname, './worker.js')

// build a set of tasks for parallelLimit to run
const tasks = Array.from({ length: limit }, (v, k) => k + 1).map((task) => {
  return cb => {
    const worker = new Worker(workerScript, { workerData: task })
    worker.on('message', (result) => { cb(null, result) })
    worker.on('error', (err) => { cb(err, null) })
  }
})

// run tasks with parallelLimit
parallelLimit(tasks, limit, (err, results) => {
  console.log('Finished with', err, results)
})
