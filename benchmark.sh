#!/bin/sh

echo "Using worker_threads"
time ./index.js

echo "Without worker_threads"
time ./benchmark.js
