# async-parallel-worker-threads

An example of using [`worker_threads`](https://nodejs.org/api/worker_threads.html) with [async.parallel](https://caolan.github.io/async/v3/docs.html#parallel).

Compare the traditional single threaded operation in `benchmark.js` with the multi-threaded operation in `index.js`.

Run a benchmark comparison between the two with `./benchmark.sh`.

See the accompanying blog post [https://tech.beyondtracks.com/posts/node-worker-threads-with-async-parallel/](https://tech.beyondtracks.com/posts/node-worker-threads-with-async-parallel/).
