#!/usr/bin/env node

// The "benchmark" operation using async.parallel[Limit] which runs on a single
// thread only, so doesn't use the full power of a multicore machine.

const os = require('os')
const parallelLimit = require('async/parallelLimit')

// our CPU intensive operation
function fibonacci(n) {
  if (n < 2) {
    return 1
  } else {
    return fibonacci(n - 2) + fibonacci(n - 1)
  }
}

// number of jobs to run will be the number of CPU cores
const limit = os.cpus().length

const fibonacciSize = 40

// build a set of tasks for parallelLimit to run
const tasks = Array.from({ length: limit }, (v, k) => k + 1).map((task) => {
  return cb => {
    const result = fibonacci(fibonacciSize)
    cb(null, result)
  }
})

// run tasks with parallelLimit
parallelLimit(tasks, limit, (err, results) => {
  console.log('Finished with', err, results)
})
