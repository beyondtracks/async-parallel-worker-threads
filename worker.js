const { parentPort, workerData, isMainThread } = require('worker_threads')

// our CPU intensive operation
function fibonacci(n) {
  if (n < 2) {
    return 1
  } else {
    return fibonacci(n - 2) + fibonacci(n - 1)
  }
}

const fibonacciSize = 40

if (!isMainThread) {
  const result = fibonacci(fibonacciSize)
  parentPort.postMessage(result)
}
